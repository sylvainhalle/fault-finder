/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.math;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;

@RunWith(Parameterized.class)
public class PowersetIteratorTest 
{
	private int iterator;
	
	public PowersetIteratorTest(int iterator)
	{
		this.iterator = iterator;
	}
	
	@Parameters
	public static Collection<?> values()
	{		
		return Arrays.asList(new Object[][]{
				{ 0 },
				{ 1 }
		});
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected PowersetIterator<?> getIterator(Set<?> set)
	{
		switch (iterator)
		{
		case 1:
			return new NegativePowersetIterator(set);
		default:
			return new PositivePowersetIterator(set);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCard0()
	{
		Set<Integer> s = new HashSet<Integer>();
		PowersetIterator<Integer> psi = (PowersetIterator<Integer>) getIterator(s);
		Set<Integer> answer;
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(0, answer.size()); // {}
		assertFalse(psi.hasNext());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCard1()
	{
		Set<Integer> s = new HashSet<Integer>();
		s.add(0);
		PowersetIterator<Integer> psi = (PowersetIterator<Integer>) getIterator(s);
		Set<Integer> answer;
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(0, answer.size()); // {}
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(1, answer.size()); // {0}
		for (Integer n : answer)
		{
			assertEquals(0, n.intValue());
		}
		assertFalse(psi.hasNext());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCard2()
	{
		Set<Integer> s = new HashSet<Integer>();
		s.add(0); s.add(1);
		PowersetIterator<Integer> psi = (PowersetIterator<Integer>) getIterator(s);
		Set<Integer> answer;
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(0, answer.size()); // {}
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(1, answer.size()); // {0}
		for (Integer n : answer)
		{
			assertEquals(0, n.intValue());
		}
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(1, answer.size()); // {1}
		for (Integer n : answer)
		{
			assertEquals(1, n.intValue());
		}
		assertTrue(psi.hasNext());
		answer = (Set<Integer>) psi.next();
		assertEquals(2, answer.size()); // {0,1}
		assertFalse(psi.hasNext());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testSetOfSet()
	{
		Set<Set<Integer>> s = new HashSet<Set<Integer>>();
		{
			Set<Integer> in_s = new HashSet<Integer>();
			in_s.add(0);
			in_s.add(1);
			s.add(in_s);
		}
		{
			Set<Integer> in_s = new HashSet<Integer>();
			in_s.add(1);
			in_s.add(2);
			s.add(in_s);
		}
		PowersetIterator<Set<Integer>> psi = (PowersetIterator<Set<Integer>>) getIterator(s);
		Set<Set<Integer>> answer;
		assertTrue(psi.hasNext());
		answer = (Set<Set<Integer>>) psi.next();
		assertEquals(0, answer.size()); // {}
		assertTrue(psi.hasNext());
		answer = (Set<Set<Integer>>) psi.next();
		assertEquals(1, answer.size()); // {{0,1}}
		assertTrue(psi.hasNext());
		answer = (Set<Set<Integer>>) psi.next();
		assertEquals(1, answer.size()); // {{1,2}}
		assertTrue(psi.hasNext());
		answer = (Set<Set<Integer>>) psi.next();
		assertEquals(2, answer.size()); // {{0,1},{1,2}}
		assertFalse(psi.hasNext());
	}

	
}
