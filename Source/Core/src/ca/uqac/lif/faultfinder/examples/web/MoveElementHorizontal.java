/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import ca.uqac.lif.faultfinder.Transformation;
import ca.uqac.lif.faultfinder.logic.NamedDomain;


public class MoveElementHorizontal extends ElementMove 
{
	public MoveElementHorizontal(String set_name, DomElement e, int new_dim)
	{
		super(set_name, e, new_dim);
	}

	@Override
	public String toString()
	{
		return "Move " + m_element.getId() + " horizontally to " + m_newDimension;  
	}

	@Override
	public boolean conflictsWith(Transformation<NamedDomain> t)
	{
		if (!(t instanceof MoveElementHorizontal))
		{
			return false;
		}
		MoveElementHorizontal m = (MoveElementHorizontal) t;
		if (m.m_element.getId() == m_element.getId())
		{
			return true;
		}
		return false;
	}

	@Override
	protected void doChange(DomElement e) 
	{
		e.setLeft(m_newDimension);		
	}
	
	@Override
	public String getExplanation()
	{
		int distance = m_newDimension - m_element.getLeft(); 
		StringBuilder message = new StringBuilder();
		message.append("This element is incorrectly placed horizontally. It should be ");
		message.append(Math.abs(distance));
		message.append(" pixels to the ");
		if (distance > 0)
		{
			message.append("right");
		}
		else
		{
			message.append("left");
		}
		return message.toString();
	}
}
