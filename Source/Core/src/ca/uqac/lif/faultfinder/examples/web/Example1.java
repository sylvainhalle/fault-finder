/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.faultfinder.FaultIterator;
import ca.uqac.lif.faultfinder.PositiveFaultIterator;
import ca.uqac.lif.faultfinder.Transformation;
import ca.uqac.lif.faultfinder.logic.Equals;
import ca.uqac.lif.faultfinder.logic.ForAll;
import ca.uqac.lif.faultfinder.logic.ConcreteNamedDomain;
import ca.uqac.lif.faultfinder.logic.NamedDomain;
import ca.uqac.lif.faultfinder.logic.VariableSymbol;

public class Example1 {

	public static void main(String[] args)
	{
		// Create set of list elements
		ConcreteNamedDomain nd = new ConcreteNamedDomain();
		{
			Set<Object> dom_elements = new HashSet<Object>();
			dom_elements.add(new DomElement("li", 10, 10, 100, 20));
			dom_elements.add(new DomElement("li", 10, 30, 100, 20));
			dom_elements.add(new DomElement("li", 15, 50, 100, 20));
			dom_elements.add(new DomElement("li", 10, 50, 100, 20));
			nd.addSet("E", dom_elements);
		}
		// Create functions over these elements
		// Create set of pixel values
		Set<Object> pixel_values = getPixelValues(nd.getSet("E"));
		// Create transformations
		Set<ElementChange> changes = new HashSet<ElementChange>();
		{
			for (Object e : nd.getSet("E"))
			{
				for (Object x : pixel_values)
				{
					changes.add(new MoveElementHorizontal("E", (DomElement) e, (int) x));
					changes.add(new MoveElementVertical("E", (DomElement) e, (int) x));
				}				
			}
		}
		// Create property: all elements have the same "left" value
		ForAll fa = new ForAll("x", 
				new ForAll("y",
						new Equals<NamedDomain>(
								new LeftFunction(new VariableSymbol("x")),
								new LeftFunction(new VariableSymbol("y"))
								), 
						"E"),
				"E");
		System.out.println(fa.satisfies(nd));
		FaultIterator<NamedDomain> it = new PositiveFaultIterator<NamedDomain>(fa, nd, changes, new DomElementFilter());
		it.setMaxSize(4);
		while (it.hasNext())
		{
			Set<? extends Transformation<NamedDomain>> fix = it.next();
			System.out.println(fix);
		}
	}
	
	public static Set<Object> getPixelValues(Set<Object> set)
	{
		Set<Object> values = new HashSet<Object>();
		for (Object o : set)
		{
			if (o instanceof DomElement)
			{
				DomElement e = (DomElement) o;
				values.add(e.getHeight());
				values.add(e.getWidth());
				values.add(e.getLeft());
				values.add(e.getTop());
			}
		}
		return values;
	}

}
