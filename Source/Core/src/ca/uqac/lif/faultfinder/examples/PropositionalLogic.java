/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.faultfinder.FaultIterator;
import ca.uqac.lif.faultfinder.Meter;
import ca.uqac.lif.faultfinder.PositiveFaultIterator;
import ca.uqac.lif.faultfinder.logic.Disjunction;
import ca.uqac.lif.faultfinder.logic.Negation;
import ca.uqac.lif.faultfinder.logic.PropositionalValuation;
import ca.uqac.lif.faultfinder.logic.PropositionalVariable;
import ca.uqac.lif.faultfinder.logic.ValuationTransformation;

public class PropositionalLogic 
{

	public static void main(String[] args) 
	{
		// a || !b
		Disjunction<PropositionalValuation> or = new Disjunction<PropositionalValuation>(
				new PropositionalVariable("a"), new Negation<PropositionalValuation>(new PropositionalVariable("b")));
		Set<ValuationTransformation> transformations = getTransformations("a", "b");
		PropositionalValuation val = new PropositionalValuation();
		val.put("a", false);
		val.put("b", true);
		FaultIterator<PropositionalValuation> it = new PositiveFaultIterator<PropositionalValuation>(or, val, transformations, new PropositionalFilter());
		Meter<PropositionalValuation> m = new Meter<PropositionalValuation>(it, 0);
		m.setCallback(new PrintCallback<PropositionalValuation>());
		m.run();
		System.out.println(m);
	}
	
	public static Set<ValuationTransformation> getTransformations(String ... variables)
	{
		Set<ValuationTransformation> out = new HashSet<ValuationTransformation>();
		for (String v : variables)
		{
			out.add(new ValuationTransformation(v, true));
			out.add(new ValuationTransformation(v, false));
		}
		return out;
	}
	
	
}
