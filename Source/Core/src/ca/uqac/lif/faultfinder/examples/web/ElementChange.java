/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.faultfinder.Transformation;
import ca.uqac.lif.faultfinder.logic.NamedDomain;

public abstract class ElementChange extends Transformation<NamedDomain> 
{
	/**
	 * The element on which to apply the transformation
	 */
	protected final DomElement m_element;
	
	/**
	 * The set on which to make the modification
	 */
	protected final String m_setName;
	
	public ElementChange(String set_id, DomElement e)
	{
		super();
		m_element = new DomElement(e);
		m_setName = set_id;
	}

	@Override
	public NamedDomain apply(NamedDomain in)
	{
		NamedDomain nd = in.clone();
		Set<Object> new_set = new HashSet<Object>();
		for (Object o : nd.getSet(m_setName))
		{
			if (!(o instanceof DomElement))
			{
				continue;
			}
			DomElement e = (DomElement) o;
			if (e.getId() != m_element.getId())
			{
				new_set.add(e);
			}
			else
			{
				DomElement new_e = new DomElement(e);
				doChange(new_e);
				new_set.add(new_e);
			}
		}
		nd.addSet(m_setName, new_set);
		return nd;
	} 
	
	protected abstract void doChange(DomElement e);
	
	public abstract String getExplanation();

}
