/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.uqac.lif.faultfinder.FaultIterator;
import ca.uqac.lif.faultfinder.Meter;
import ca.uqac.lif.faultfinder.Meter.SolutionCallback;
import ca.uqac.lif.faultfinder.PositiveFaultIterator;
import ca.uqac.lif.faultfinder.Transformation;
import ca.uqac.lif.faultfinder.logic.ConcreteNamedDomain;
import ca.uqac.lif.faultfinder.logic.Conjunction;
import ca.uqac.lif.faultfinder.logic.Disjunction;
import ca.uqac.lif.faultfinder.logic.ForAll;
import ca.uqac.lif.faultfinder.logic.Function;
import ca.uqac.lif.faultfinder.logic.NamedDomain;
import ca.uqac.lif.faultfinder.logic.Negation;
import ca.uqac.lif.faultfinder.logic.RoteFunction;
import ca.uqac.lif.faultfinder.logic.FunctionTransformation;
import ca.uqac.lif.faultfinder.logic.PredicateVariable;
import ca.uqac.lif.util.FileReadWrite;

public class Graph 
{

	public static void main(String[] args)
	{
		ConcreteNamedDomain nd = new ConcreteNamedDomain();
		{
			Set<Object> domain = new HashSet<Object>();
			domain.add(1); domain.add(2); domain.add(3); domain.add(4); domain.add(5);
			nd.addSet("S", domain);
		}
		{
			// p: adjacency matrix
			Object[][] inputs = {{1,1}, {1,2}, {1,3}, {1,4}, {1,5},
					{2,1}, {2,2}, {2,3}, {2,4}, {2,5},
					{3,1}, {3,2}, {3,3}, {3,4}, {3,5},
					{4,1}, {4,2}, {4,3}, {4,4}, {4,5},
					{5,1}, {5,2}, {5,3}, {5,4}, {5,5}};
			Object[] values = {false, true, true, false, false,
					true, false, false, true, false,
					true, false, false, true, false,
					false, true, true, false, true,
					false, false, false, true, false};
			RoteFunction p = new RoteFunction(2, inputs, values);
			nd.addFunction("p", p);
		}
		{
			// q1 : yellow
			Integer[][] inputs = {{1},{2},{3},{4},{5}};
			Boolean[] values = {true, false, false, true, true};
			RoteFunction q = new RoteFunction(1, inputs, values);
			nd.addFunction("q1", q);
		}
		{
			// q2 : green
			Integer[][] inputs = {{1},{2},{3},{4},{5}};
			Boolean[] values = {false, true, false, false, false};
			RoteFunction q = new RoteFunction(1, inputs, values);
			nd.addFunction("q2", q);
		}
		{
			// q3 : red
			Integer[][] inputs = {{1},{2},{3},{4},{5}};
			Boolean[] values = {false, false, true, false, false};
			RoteFunction q = new RoteFunction(1, inputs, values);
			nd.addFunction("q3", q);
		}
		// Create transformations
		Set<Transformation<NamedDomain>> transformations = new HashSet<Transformation<NamedDomain>>();
		{
			for (Object e1 : nd.getSet("S"))
			{
				for (int i = 1; i <= 3; i++)
				{
					transformations.add(new ColourChange((int) e1, i));
				}
			}
			for (Object e1 : nd.getSet("S"))
			{
				for (Object e2 : nd.getSet("S"))
				{
					if ((int) e2 > (int) e1)
					{
						transformations.add(new EdgeChange((int) e1, (int) e2, false));
					}
				}
			}
		}
		// All assertions
		Conjunction<NamedDomain> big_and = new Conjunction<NamedDomain>();
		// Assertion 1: all vertices have exactly one colour
		/*{
			ForAll fa = new ForAll("x",
					new Disjunction<PredicateSet>(
							new Conjunction<PredicateSet>(
									new PredicateVariable("q1", "x"),
									new Negation<PredicateSet>(new PredicateVariable("q2", "x")),
									new Negation<PredicateSet>(new PredicateVariable("q3", "x"))),
							new Conjunction<PredicateSet>(
									new PredicateVariable("q2", "x"),
									new Negation<PredicateSet>(new PredicateVariable("q1", "x")),
									new Negation<PredicateSet>(new PredicateVariable("q3", "x"))),									
							new Conjunction<PredicateSet>(
									new PredicateVariable("q3", "x"),
									new Negation<PredicateSet>(new PredicateVariable("q1", "x")),
									new Negation<PredicateSet>(new PredicateVariable("q2", "x")))), 
							domain);
			big_and.addOperand(fa);
		}*/
		// Assertion 2: the adjacency matrix is symmetric
		/*{
			ForAll fa = new ForAll("x", 
					new ForAll("y",
							new Disjunction<PredicateSet>(new Negation<PredicateSet>(new PredicateVariable("p", "x", "y")), new PredicateVariable("p", "y", "x")), 
							domain),
					domain);
			big_and.addOperand(fa);
		}*/
		// Assertion 3: two neighbours have different colours
		{
			ForAll fa = new ForAll("x", 
					new ForAll("y",
							new Disjunction<NamedDomain>(
									new Negation<NamedDomain>(new PredicateVariable("p", "x", "y")),
									new Conjunction<NamedDomain>(
											new Disjunction<NamedDomain>(
													new Negation<NamedDomain>(new PredicateVariable("q1", "x")),
													new Negation<NamedDomain>(new PredicateVariable("q1", "y"))),
												new Disjunction<NamedDomain>(
													new Negation<NamedDomain>(new PredicateVariable("q2", "x")),
													new Negation<NamedDomain>(new PredicateVariable("q2", "y"))),
												new Disjunction<NamedDomain>(
													new Negation<NamedDomain>(new PredicateVariable("q3", "x")),
													new Negation<NamedDomain>(new PredicateVariable("q3", "y")))
									)),
							"S"),
					"S");
			big_and.addOperand(fa);
		}
		System.out.println(big_and.satisfies(nd));
		FaultIterator<NamedDomain> it = new PositiveFaultIterator<NamedDomain>(big_and, nd, transformations, new FirstOrderFilter());
		Meter<NamedDomain> m = new Meter<NamedDomain>(it, 6);
		//m.setCallback(new PrintCallback<PredicateSet>());
		m.setCallback(new GraphCallback(nd));
		m.run();
		System.out.println(m);
	}
	
	public static class GraphCallback extends SolutionCallback<NamedDomain>
	{
		protected final NamedDomain m_originalGraph;
		
		protected int m_solutionCount = 0;
		
		public GraphCallback(NamedDomain original_graph)
		{
			super();
			m_originalGraph = original_graph;
		}

		@Override
		public void doWith(Set<? extends Transformation<NamedDomain>> transformations) 
		{
			m_solutionCount++;
			// Compute the repaired graph
			NamedDomain repair = m_originalGraph;
			for (Transformation<NamedDomain> t : transformations)
			{
				repair = t.apply(repair);
			}
			String output = Graph.getDotGraph(repair);
			// Write that to a file
			String filename = "repair-" + m_solutionCount + ".dot";
			try 
			{
				FileReadWrite.writeToFile(filename, output);
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
		
	}
	
	public static class ColourChange extends FunctionTransformation
	{
		public ColourChange(int vertex, int colour)
		{
			super();
			// Set selected colour to true
			m_changes.add(new FunctionChange("q" + colour, true, vertex));
			for (int i = 1; i <= 3; i++)
			{
				// Set others to false
				if (i != colour)
				{
					m_changes.add(new FunctionChange("q" + i, false, vertex));
				}
			}
		}
	}
	
	public static class EdgeChange extends FunctionTransformation
	{
		public EdgeChange(int vertex1, int vertex2, boolean add)
		{
			super();
			m_changes.add(new FunctionChange("p", add, vertex1, vertex2));
			m_changes.add(new FunctionChange("p", add, vertex2, vertex1));
		}
	}
	
	public static String getDotGraph(NamedDomain ps)
	{
		StringBuilder out = new StringBuilder();
		out.append("graph G {\n");
		out.append("  node [shape=circle,style=filled];\n");
		Function p = ps.getFunction("p");
		for (int i = 1; i <= 5; i++)
		{
			for (int j = i + 1; j <= 5; j++)
			{
				List<Object> in = new ArrayList<Object>(2);
				in.add(i);
				in.add(j);
				if ((boolean) p.evaluate(in))
				{
					out.append("  ").append(i).append("--").append(j).append(";\n");
				}
			}
		}
		for (int i = 1; i <= 3; i++)
		{
			Function qi = ps.getFunction("q" + i);
			for (int j = 1; j <= 5; j++)
			{
				List<Object> in = new ArrayList<Object>(1);
				in.add(j);
				if ((boolean) qi.evaluate(in))
				{
					out.append("  ").append(j).append(" [label=\"").append(j).append("\",fillcolor=\"").append(getColourName(i)).append("\"];\n");
				}
			}
		}
		out.append("}\n");
		return out.toString();
	}
	
	public static String getColourName(int n)
	{
		String colour = "";
		switch (n)
		{
		case 1:
			colour = "yellow";
			break;
		case 2:
			colour = "green";
			break;
		case 3:
			colour = "red";
			break;
		default:
			colour = "white";
			break;
		}
		return colour;
	}
}
