/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.faultfinder.FaultIterator;
import ca.uqac.lif.faultfinder.Meter;
import ca.uqac.lif.faultfinder.PositiveFaultIterator;
import ca.uqac.lif.faultfinder.logic.ForAll;
import ca.uqac.lif.faultfinder.logic.ConcreteNamedDomain;
import ca.uqac.lif.faultfinder.logic.NamedDomain;
import ca.uqac.lif.faultfinder.logic.RoteFunction;
import ca.uqac.lif.faultfinder.logic.FunctionTransformation;
import ca.uqac.lif.faultfinder.logic.PredicateVariable;

public class FirstOrderLogic
{
	public static void main(String[] args)
	{
		NamedDomain nd = new ConcreteNamedDomain();
		{
			Set<Object> domain = new HashSet<Object>();
			domain.add(0); domain.add(1);
			nd.addSet("S", domain);
		}
		{
			// p is a unary predicate
			Integer[][] inputs = {{0}, {1}};
			Boolean[] values = {false, false};
			RoteFunction p = new RoteFunction(1, inputs, values);
			nd.addFunction("p", p);
		}
		{
			// q is a binary predicate
			Integer[][] inputs = {{0,0},{0,1},{1,0},{1,1}};
			Boolean[] values = {false, false, false, false};
			RoteFunction q = new RoteFunction(2, inputs, values);
			nd.addFunction("q", q);
		}
		// Create transformations
		Set<FunctionTransformation> transformations = new HashSet<FunctionTransformation>();
		{
			for (Object e1 : nd.getSet("S"))
			{
				{
					FunctionTransformation pt = new FunctionTransformation();
					pt.addChange("p", true, e1);
					transformations.add(pt);
				}
				{
					FunctionTransformation pt = new FunctionTransformation();
					pt.addChange("p", false, e1);
					transformations.add(pt);
				}
			}
		}
		// forall x : p(x)
		ForAll fa = new ForAll("x", new PredicateVariable("p", "x"), "S");
		System.out.println(fa.satisfies(nd));
		FaultIterator<NamedDomain> it = new PositiveFaultIterator<NamedDomain>(fa, nd, transformations, new FirstOrderFilter());
		Meter<NamedDomain> m = new Meter<NamedDomain>(it, 0);
		m.setCallback(new PrintCallback<NamedDomain>());
		m.run();
		System.out.println(m);
	}

}
