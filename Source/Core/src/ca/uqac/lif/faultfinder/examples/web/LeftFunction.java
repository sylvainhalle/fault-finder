/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import ca.uqac.lif.faultfinder.logic.Evaluable;
import ca.uqac.lif.faultfinder.logic.Function;
import ca.uqac.lif.faultfinder.logic.LogicalExpressionVisitor;

public class LeftFunction extends Function 
{
	protected Evaluable m_argument;
	
	public LeftFunction(Evaluable argument)
	{
		super();
		m_argument = argument.getCopy();
	}
	
	@Override
	public Object evaluate(Object o)
	{
		DomElement e = (DomElement) m_argument.evaluate(o);
		return e.getLeft();
	}
	
	@Override
	public int getArity()
	{
		return 1;
	}
	
	@Override
	public String toString()
	{
		return "left";
	}
	
	@Override
	public LeftFunction getCopy()
	{
		return new LeftFunction(m_argument);
	}

	@Override
	public void replaceAll(String name, Object value) 
	{
		m_argument.replaceAll(name, value);
	}
	
	@Override
	public void accept(LogicalExpressionVisitor<?> visitor) 
	{
		visitor.visit(this);
	}
}
