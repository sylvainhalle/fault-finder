/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DomElement
{
	private final int m_id;
	
	private static int s_idCount = 0;
	
	private static final Lock s_idLock = new ReentrantLock();

	private int m_left = 0;
	
	private int m_top = 0;
	
	private int m_width = 0;
	
	private int m_height = 0;
	
	private final String m_tagName;
	
	public DomElement(String tag_name)
	{
		super();
		s_idLock.lock();
		m_id = s_idCount++;
		s_idLock.unlock();
		m_tagName = tag_name;
	}
	
	private DomElement(String tag_name, int id)
	{
		super();
		m_id = id;
		m_tagName = tag_name;
	}
	
	public DomElement(DomElement e)
	{
		this(e.m_tagName, e.m_id);
		m_left = e.m_left;
		m_top = e.m_top;
		m_height = e.m_height;
		m_width = e.m_width;
	}
	
	public DomElement(String tag_name, int left, int top, int width, int height)
	{
		this(tag_name);
		m_left = left;
		m_top = top;
		m_width = width;
		m_height = height;
	}
	
	public int getId()
	{
		return m_id;
	}
	
	public int getLeft() 
	{
		return m_left;
	}

	public DomElement setLeft(int left) 
	{
		this.m_left = left;
		return this;
	}

	public int getTop() 
	{
		return m_top;
	}

	public DomElement setTop(int top) 
	{
		this.m_top = top;
		return this;
	}
	
	public int getWidth()
	{
		return m_width;
	}
	
	public DomElement setWidth(int width)
	{
		m_width = width;
		return this;
	}
	
	public int getHeight()
	{
		return m_height;
	}
	
	public DomElement setHeight(int height)
	{
		m_height = height;
		return this;
	}
	
	public String getTagName()
	{
		return m_tagName;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o == null || !(o instanceof DomElement))
		{
			return false;
		}
		return ((DomElement) o).m_id == m_id;
	}
	
	@Override
	public int hashCode()
	{
		return m_id;
	}
	
	@Override
	public String toString()
	{
		return "id: " + m_id + ", left:" + m_left;
	}
	
}
