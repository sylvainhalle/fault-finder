/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples;

import java.util.HashSet;
import java.util.Set;

public class SimpleDom {

	public static void main(String[] args)
	{
		Set<Object> elements = new HashSet<Object>();
		elements.add("e1");
		elements.add("e2");
		elements.add("e3");
		Set<Object> pixel_values = new HashSet<Object>();
		pixel_values.add(100);
		pixel_values.add(110);
		pixel_values.add(200);
		
	}

}
