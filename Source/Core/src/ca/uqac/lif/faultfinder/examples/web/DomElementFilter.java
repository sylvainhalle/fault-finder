/*
    Automated fault localization in discrete structures
    Copyright (C) 2006-2017 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ca.uqac.lif.faultfinder.examples.web;

import java.util.Set;

import ca.uqac.lif.math.PositivePowersetIterator;

public class DomElementFilter implements PositivePowersetIterator.SolutionFilter<ElementChange>
{

	@Override
	public boolean canAdd(Set<? extends ElementChange> set, ElementChange change)
	{
		for (ElementChange c : set)
		{
			if (c.conflictsWith(change))
			{
				return false;
			}
		}
		return true;
	}

}
