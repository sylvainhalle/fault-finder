package ca.uqac.lif.faultfinder.logic;

public interface LogicalExpressionVisitor<T> 
{
	public void visit(LogicalExpression<T> expression);
	
	public void visit(Evaluable e);
}
