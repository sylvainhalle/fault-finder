\documentclass{article}
\usepackage{fullpage,graphicx,subfig}
\begin{document}
\begin{figure}
\subfloat[Original]{\includegraphics[scale=0.5]{original}}
<?php
$scale = 0.5;
$per_col = 3;
$per_page = $per_col * 4;
for ($i = 1; $i <= 17; $i++)
{
  if ($i != 1 && ($i - 0) % $per_col == 0)
  {
    echo "\\\\\n";
  }
  if ($i != 1 && ($i - 0) % $per_page == 0)
  {
  	  echo "\\end{figure}\n\\begin{figure}\n";
  }
  echo "\\subfloat[$i]{";
  echo "\\includegraphics[scale=".$scale."]{repair-".$i."}";
  echo "}\n";
}
?>
\end{figure}
\end{document}