#! /usr/bin/zsh

for dotfile in *.dot; do
  if [ ! -f ${dotfile:r}.pdf ]; then
    echo $dotfile
    neato -Tpdf $dotfile > ${dotfile:r}.pdf
  fi
done
echo Concatenating to all.pdf
pdftk *.pdf cat output all.pdf